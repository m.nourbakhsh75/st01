/*
 * This file was automatically generated by EvoSuite
 * Thu Jun 23 13:27:27 GMT 2022
 */

package ir.ramtung.impl2;

import org.junit.Test;
import static org.junit.Assert.*;
import ir.ramtung.impl2.Book;
import ir.ramtung.impl2.Magazine;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true, useJEE = true) 
public class Document_ESTest extends Document_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test00()  throws Throwable  {
      Book book0 = new Book("", 0);
      book0.title = null;
      book0.getTitle();
      assertEquals(0, book0.getCopies());
  }

  @Test(timeout = 4000)
  public void test01()  throws Throwable  {
      Book book0 = new Book("", 0);
      String string0 = book0.getTitle();
      assertNotNull(string0);
      assertEquals(0, book0.getCopies());
  }

  @Test(timeout = 4000)
  public void test02()  throws Throwable  {
      Book book0 = new Book("ir.ramtung.impl2.Book", 1457);
      book0.day = 0;
      int int0 = book0.getDay();
      assertEquals(0, int0);
  }

  @Test(timeout = 4000)
  public void test03()  throws Throwable  {
      Magazine magazine0 = new Magazine((String) null, 3230, (-2672), 3230);
      magazine0.day = (-2672);
      int int0 = magazine0.getDay();
      assertEquals((-2672), int0);
  }

  @Test(timeout = 4000)
  public void test04()  throws Throwable  {
      Magazine magazine0 = new Magazine("Qv", 0, 0, 0);
      int int0 = magazine0.getCopies();
      assertEquals(0, int0);
  }

  @Test(timeout = 4000)
  public void test05()  throws Throwable  {
      Book book0 = new Book("", 0);
      book0.returnBook();
      int int0 = book0.getCopies();
      assertEquals(1, int0);
  }

  @Test(timeout = 4000)
  public void test06()  throws Throwable  {
      Magazine magazine0 = new Magazine("", 2869, 253, 2869);
      magazine0.calculatePenalty(1273);
      assertEquals(2869, magazine0.getCopies());
  }

  @Test(timeout = 4000)
  public void test07()  throws Throwable  {
      Book book0 = new Book("V", (-2546));
      int int0 = book0.getCopies();
      assertEquals((-2546), int0);
  }

  @Test(timeout = 4000)
  public void test08()  throws Throwable  {
      Book book0 = new Book("V", (-2546));
      String string0 = book0.getTitle();
      assertEquals((-2546), book0.getCopies());
      assertNotNull(string0);
  }

  @Test(timeout = 4000)
  public void test09()  throws Throwable  {
      Magazine magazine0 = new Magazine((String) null, (-35), (-35), (-35));
      int int0 = magazine0.getDay();
      assertEquals((-35), magazine0.getCopies());
      assertEquals(2, int0);
  }

  @Test(timeout = 4000)
  public void test10()  throws Throwable  {
      Book book0 = new Book("V", (-2546));
      book0.barrowBook();
      assertEquals((-2547), book0.getCopies());
  }
}
