import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest2 {

    public static boolean debug = false;

    @Test
    public void test1001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1001");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        library0.timePass((int) (short) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1002");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        library0.timePass((int) (byte) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1003");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1004");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (short) 1, (int) (short) 0, 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1005");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (short) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1006");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addMagazine("hi!", (int) ' ', (int) (byte) 1, (int) (byte) 1);
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1007");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        int int9 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int12 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1008");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1009");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        int int8 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1010");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
    }

    @Test
    public void test1011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1011");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1012");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            int int22 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1013");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '4');
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1014");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", 1, (-1), 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative number");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1015");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        int int9 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1016");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1017");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1018");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1019");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (short) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1020");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", 0, (int) '4', (int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1021");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", 100, 1, (int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1022");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        library0.timePass((int) 'a');
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1023");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1024");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(100);
        library0.addReference("hi!", (int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1025");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1026");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addBook("hi!", (int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            int int9 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1027");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        java.util.List<java.lang.String> strList1 = library0.availableTitles();
        library0.timePass((int) (byte) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList1);
    }

    @Test
    public void test1028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1028");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.lang.Class<?> wildcardClass11 = strList10.getClass();
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1029");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1030");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1031");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.addMagazine("hi!", (int) (byte) 100, 1, (int) (byte) 100);
        java.util.List<java.lang.String> strList18 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList18);
    }

    @Test
    public void test1032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1032");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.addReference("hi!", 100);
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1033");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        int int9 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (short) 0, (-76000), (int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1034");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1035");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", 0, (int) (byte) 0, (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1036");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.lang.Class<?> wildcardClass16 = strList15.getClass();
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1037");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        int int8 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1038");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1039");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1040");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1041");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int11 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1042");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1043");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        int int20 = library0.getTotalPenalty("hi!");
        java.lang.Class<?> wildcardClass21 = library0.getClass();
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20 == 0);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test1044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1044");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1045");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1046");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1047");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        int int12 = library0.getTotalPenalty("hi!");
        library0.addMagazine("hi!", (int) '4', (int) (short) 1, (int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12 == 0);
    }

    @Test
    public void test1048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1048");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (-76000), (int) '4', (int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1049");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) ' ', 0, (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative number");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1050");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1051");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        library0.timePass((int) (byte) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) ' ', 1, (int) (short) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1052");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        int int8 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1053");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1054");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.timePass(1);
        java.lang.Class<?> wildcardClass19 = library0.getClass();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1055");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1056");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", 100, (int) (short) -1, (int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative number");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1057");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1058");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1059");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList20 = library0.availableTitles();
        java.util.List<java.lang.String> strList21 = library0.availableTitles();
        java.lang.Class<?> wildcardClass22 = strList21.getClass();
        org.junit.Assert.assertNotNull(strList20);
        org.junit.Assert.assertNotNull(strList21);
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test1060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1060");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(100);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1061");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (short) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1062");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1063");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.lang.Class<?> wildcardClass12 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1064");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        java.lang.Class<?> wildcardClass8 = library0.getClass();
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1065");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1066");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass((int) 'a');
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1067");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) (byte) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1068");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1069");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1070");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (byte) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1071");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1072");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList22);
    }

    @Test
    public void test1073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1073");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        int int8 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1074");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        int int8 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1075");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1076");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.timePass(10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 100, (int) '4', (int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1077");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.timePass(10);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1078");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.CannotExtendEx; message: Cannot extend a late loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1079");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            int int20 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1080");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1081");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1082");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        int int18 = library0.getTotalPenalty("hi!");
        java.lang.Class<?> wildcardClass19 = library0.getClass();
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1083");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1084");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) '#', (int) (byte) 100, (int) (byte) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1085");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        int int8 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertNotNull(strList9);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1086");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1087");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) 'a');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1088");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        library0.timePass(0);
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1089");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass(10);
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        int int24 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-76000) + "'", int24 == (-76000));
    }

    @Test
    public void test1090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1090");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1091");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (short) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1092");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1093");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        java.lang.Class<?> wildcardClass10 = library0.getClass();
        org.junit.Assert.assertNotNull(strList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1094");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        java.util.List<java.lang.String> strList18 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertNotNull(strList18);
    }

    @Test
    public void test1095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1095");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.lang.Class<?> wildcardClass9 = strList8.getClass();
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1096");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) (byte) 1);
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((-76000));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
    }

    @Test
    public void test1097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1097");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
    }

    @Test
    public void test1098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1098");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1099");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        int int11 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((-1));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
    }

    @Test
    public void test1100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1100");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (byte) -1, (int) (short) 0, 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1101");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1102");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1103");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.lang.Class<?> wildcardClass9 = library0.getClass();
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1104");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 10);
        library0.timePass((int) ' ');
        library0.borrow("hi!", "hi!");
        java.lang.Class<?> wildcardClass27 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test1105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1105");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (short) 1, (int) (byte) 1, (int) (short) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1106");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        java.lang.Class<?> wildcardClass14 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test1107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1107");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        java.util.List<java.lang.String> strList1 = library0.availableTitles();
        library0.timePass((int) (byte) 0);
        java.lang.Class<?> wildcardClass4 = library0.getClass();
        org.junit.Assert.assertNotNull(strList1);
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1108");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        int int9 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1109");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1110");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) 'a', (int) (short) 100, (int) (short) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1111");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        int int9 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1112");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) ' ');
        int int14 = library0.getTotalPenalty("hi!");
        java.lang.Class<?> wildcardClass15 = library0.getClass();
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1113");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (short) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1114");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1115");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1116");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1117");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        int int11 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
    }

    @Test
    public void test1118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1118");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1119");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1120");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1121");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1122");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        java.util.List<java.lang.String> strList1 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (short) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList1);
    }

    @Test
    public void test1123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1123");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1124");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
    }

    @Test
    public void test1125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1125");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1126");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1127");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1128");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1129");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1130");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        int int9 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1131");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1132");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1133");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1134");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((-76000));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1135");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1136");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1137");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1138");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        int int18 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
    }

    @Test
    public void test1139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1139");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1140");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1141");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1142");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.lang.Class<?> wildcardClass12 = strList11.getClass();
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1143");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        int int8 = library0.getTotalPenalty("hi!");
        library0.addMagazine("hi!", 100, (int) ' ', (int) (short) 100);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1144");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(100);
        library0.addReference("hi!", (int) (short) 1);
        int int11 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
    }

    @Test
    public void test1145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1145");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        library0.timePass((int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1146");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        int int15 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", 1, (int) '#', 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1147");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1148");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass(10);
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList22);
    }

    @Test
    public void test1149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1149");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", 0, (int) ' ', 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1150");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1151");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        int int9 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1152");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addMagazine("hi!", (int) ' ', (int) (byte) 1, (int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", 0, 100, 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1153");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(100);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1154");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1155");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
    }

    @Test
    public void test1156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1156");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1157");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1158");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1159");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1160");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (byte) -1, (int) (short) 1, (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
    }

    @Test
    public void test1161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1161");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.addMagazine("hi!", (int) (byte) 100, 1, (int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1162");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        library0.timePass((int) (short) 10);
        library0.addBook("hi!", (int) '#');
        java.lang.Class<?> wildcardClass12 = library0.getClass();
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test1163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1163");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        java.lang.Class<?> wildcardClass4 = library0.getClass();
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertNotNull(wildcardClass4);
    }

    @Test
    public void test1164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1164");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        library0.timePass(0);
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        int int18 = library0.getTotalPenalty("hi!");
        java.lang.Class<?> wildcardClass19 = library0.getClass();
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1165");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            int int16 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
    }

    @Test
    public void test1166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1166");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        int int13 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '4');
        java.lang.Class<?> wildcardClass16 = library0.getClass();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1167");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (byte) 10, (int) (byte) 0, 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative number");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1168");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        library0.timePass((int) (short) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1169");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        int int8 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 100, (int) ' ', (-1));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1170");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        java.util.List<java.lang.String> strList18 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertNotNull(strList18);
    }

    @Test
    public void test1171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1171");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        library0.timePass(0);
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        int int18 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
    }

    @Test
    public void test1172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1172");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        library0.timePass(0);
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        int int18 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList19 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
        org.junit.Assert.assertNotNull(strList19);
    }

    @Test
    public void test1173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1173");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 10);
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1174");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        java.lang.Class<?> wildcardClass15 = strList14.getClass();
        org.junit.Assert.assertNotNull(strList14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1175");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        int int11 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
    }

    @Test
    public void test1176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1176");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        int int16 = library0.getTotalPenalty("hi!");
        library0.addReference("hi!", (int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16 == 0);
    }

    @Test
    public void test1177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1177");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.addReference("hi!", 100);
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1178");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1179");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        int int16 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16 == 0);
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1180");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        java.util.List<java.lang.String> strList1 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList1);
    }

    @Test
    public void test1181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1181");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        int int18 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList19 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
        org.junit.Assert.assertNotNull(strList19);
    }

    @Test
    public void test1182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1182");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
    }

    @Test
    public void test1183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1183");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1184");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", 0, (int) (byte) 1, (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1185");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 0);
    }

    @Test
    public void test1186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1186");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1187");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        int int18 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
    }

    @Test
    public void test1188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1188");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1189");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1190");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '4');
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
    }

    @Test
    public void test1191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1191");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1192");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass((int) 'a');
        library0.timePass((int) (short) 0);
    }

    @Test
    public void test1193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1193");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        int int9 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1194");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) '#');
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.lang.Class<?> wildcardClass7 = library0.getClass();
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test1195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1195");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(100);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1196");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1197");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1198");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.addReference("hi!", 100);
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (byte) 0, (int) 'a', (-76000));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1199");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addMagazine("hi!", (int) ' ', (int) (byte) 1, (int) (byte) 1);
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass(10);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1200");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        int int9 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1201");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 10);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (short) 1, (int) 'a', (int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1202");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addBook("hi!", (int) (byte) 100);
        java.lang.Class<?> wildcardClass8 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1203");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        int int13 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        library0.timePass((int) (byte) 10);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1204");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass((int) (short) 1);
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1205");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1206");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.lang.Class<?> wildcardClass11 = library0.getClass();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1207");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            int int11 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1208");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1209");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        library0.timePass((int) (short) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (byte) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1210");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1211");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1212");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
    }

    @Test
    public void test1213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1213");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass((int) '#');
        library0.timePass((int) (byte) 10);
        java.util.List<java.lang.String> strList24 = library0.availableTitles();
        library0.timePass((int) (byte) 10);
        org.junit.Assert.assertNotNull(strList24);
    }

    @Test
    public void test1214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1214");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1215");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) '#', (int) (short) 100, (int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1216");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1217");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1218");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1219");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.timePass(10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 0, (int) (byte) 1, (int) 'a');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1220");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass((int) '#');
        library0.timePass((int) (byte) 10);
        java.util.List<java.lang.String> strList24 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList24);
    }

    @Test
    public void test1221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1221");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1222");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (short) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
    }

    @Test
    public void test1223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1223");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int7 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1224");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass((int) '4');
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1225");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        int int18 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
    }

    @Test
    public void test1226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1226");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass((int) '4');
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1227");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1228");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.lang.Class<?> wildcardClass8 = strList7.getClass();
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1229");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        int int13 = library0.getTotalPenalty("hi!");
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1230");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1231");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        java.lang.Class<?> wildcardClass23 = strList22.getClass();
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test1232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1232");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (-76000));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1233");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList20 = library0.availableTitles();
        java.lang.Class<?> wildcardClass21 = strList20.getClass();
        org.junit.Assert.assertNotNull(strList20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test1234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1234");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.lang.Class<?> wildcardClass9 = library0.getClass();
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1235");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        library0.timePass((int) (short) 0);
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        java.util.List<java.lang.String> strList18 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) ' ', (-76000), (-1));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative number");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList17);
        org.junit.Assert.assertNotNull(strList18);
    }

    @Test
    public void test1236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1236");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (short) -1, (int) ' ', 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1237");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        library0.timePass((int) (short) 10);
        library0.addReference("hi!", 10);
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1238");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1239");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        library0.timePass((int) (short) 0);
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        java.util.List<java.lang.String> strList18 = library0.availableTitles();
        java.lang.Class<?> wildcardClass19 = library0.getClass();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList17);
        org.junit.Assert.assertNotNull(strList18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1240");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass((int) 'a');
        java.lang.Class<?> wildcardClass5 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test1241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1241");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        library0.addMagazine("hi!", 10, (int) (short) 1, (int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1242");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1243");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 100);
        library0.timePass((int) '4');
    }

    @Test
    public void test1244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1244");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (short) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1245");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1246");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        library0.timePass((int) (short) 1);
        java.lang.Class<?> wildcardClass9 = library0.getClass();
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1247");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) ' ', (int) (short) 1, 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1248");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        library0.addBook("hi!", (int) '4');
        library0.timePass((int) (short) 10);
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1249");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.addReference("hi!", 100);
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        int int13 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1250");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass((int) (short) 1);
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1251");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        java.util.List<java.lang.String> strList23 = library0.availableTitles();
        java.util.List<java.lang.String> strList24 = library0.availableTitles();
        java.util.List<java.lang.String> strList25 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertNotNull(strList23);
        org.junit.Assert.assertNotNull(strList24);
        org.junit.Assert.assertNotNull(strList25);
    }

    @Test
    public void test1252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1252");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.addReference("hi!", (int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
    }

    @Test
    public void test1253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1253");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1254");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1255");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        int int18 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.CannotExtendEx; message: Cannot extend on the same day borrowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
    }

    @Test
    public void test1256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1256");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (byte) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1257");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
    }

    @Test
    public void test1258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1258");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.timePass((int) '4');
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (byte) -1, (int) ' ', (int) (short) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1259");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.addBook("hi!", (int) (short) 10);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1260");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1261");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (short) 0, (-76000), (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1262");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList20 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (-1), (int) (short) 0, 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList20);
    }

    @Test
    public void test1263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1263");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        int int17 = library0.getTotalPenalty("hi!");
        library0.timePass((int) (byte) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1264");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass((int) (short) 1);
        library0.timePass((int) (short) 0);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1265");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.lang.Class<?> wildcardClass9 = library0.getClass();
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1266");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList22);
    }

    @Test
    public void test1267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1267");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) (byte) 1);
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
    }

    @Test
    public void test1268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1268");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        int int12 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (short) 0, (int) (short) 100, (int) (short) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12 == 0);
    }

    @Test
    public void test1269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1269");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1270");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1271");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        int int9 = library0.getTotalPenalty("hi!");
        int int11 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
    }

    @Test
    public void test1272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1272");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        int int13 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1273");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", 100, (int) (short) 10, (int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1274");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1275");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.timePass((int) (byte) 0);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1276");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.borrow("hi!", "hi!");
    }

    @Test
    public void test1277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1277");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int11 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1278");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        java.util.List<java.lang.String> strList23 = library0.availableTitles();
        java.util.List<java.lang.String> strList24 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertNotNull(strList23);
        org.junit.Assert.assertNotNull(strList24);
    }

    @Test
    public void test1279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1279");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1280");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", 100);
    }

    @Test
    public void test1281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1281");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        int int18 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
    }

    @Test
    public void test1282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1282");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1283");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addMagazine("hi!", (int) ' ', (int) (byte) 1, (int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) 'a', (int) (short) 0, (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative number");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1284");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass((int) '4');
        library0.timePass(1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1285");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        library0.timePass((int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1286");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        int int9 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((-76000));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1287");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        library0.timePass((int) (short) 1);
        library0.addReference("hi!", (int) ' ');
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
    }

    @Test
    public void test1288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1288");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 100, (int) (byte) 10, (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1289");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 10);
        library0.timePass((int) ' ');
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1290");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1291");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (short) 100, (int) 'a', (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1292");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) 'a');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1293");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1294");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        int int15 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        java.lang.Class<?> wildcardClass17 = strList16.getClass();
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test1295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1295");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.addReference("hi!", (int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 0, (int) (short) 100, (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
    }

    @Test
    public void test1296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1296");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList20 = library0.availableTitles();
        java.util.List<java.lang.String> strList21 = library0.availableTitles();
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList20);
        org.junit.Assert.assertNotNull(strList21);
        org.junit.Assert.assertNotNull(strList22);
    }

    @Test
    public void test1297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1297");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int15 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1298");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
    }

    @Test
    public void test1299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1299");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        int int15 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1300");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int18 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1301");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        int int8 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertNotNull(strList9);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1302");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (-76000));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1303");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.lang.Class<?> wildcardClass9 = library0.getClass();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test1304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1304");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        int int16 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int19 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16 == 0);
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1305");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        library0.timePass((int) (short) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) (short) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1306");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (short) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1307");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1308");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        java.lang.Class<?> wildcardClass21 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test1309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1309");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1310");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        int int12 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12 == 0);
    }

    @Test
    public void test1311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1311");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.timePass((int) 'a');
    }

    @Test
    public void test1312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1312");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1313");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1314");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        int int18 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18 == 0);
    }

    @Test
    public void test1315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1315");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass((int) '#');
        java.lang.Class<?> wildcardClass16 = library0.getClass();
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1316");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1317");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        int int8 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (short) 100, (int) (short) 1, (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertNotNull(strList9);
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1318");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1319");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        library0.timePass((int) (short) 0);
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1320");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList14);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1321");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.timePass((int) (byte) 10);
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1322");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1323");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        int int8 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int11 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1324");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        int int8 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) 'a', (int) 'a', (-1));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1325");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        int int9 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1326");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        int int19 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList17);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19 == 0);
    }

    @Test
    public void test1327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1327");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass((int) ' ');
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        library0.timePass(100);
        int int20 = library0.getTotalPenalty("hi!");
        org.junit.Assert.assertNotNull(strList16);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20 == 0);
    }

    @Test
    public void test1328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1328");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        java.util.List<java.lang.String> strList1 = library0.availableTitles();
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList1);
    }

    @Test
    public void test1329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1329");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        int int9 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1330");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1331");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            int int13 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1332");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) ' ');
        int int14 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
    }

    @Test
    public void test1333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1333");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.lang.Class<?> wildcardClass7 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test1334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1334");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1335");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1336");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1337");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) '#');
    }

    @Test
    public void test1338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1338");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 10, (int) (short) -1, 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1339");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", 10);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1340");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass(10);
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        int int24 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-76000) + "'", int24 == (-76000));
    }

    @Test
    public void test1341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1341");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.timePass((int) ' ');
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1342");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1343");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1344");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        library0.addProfMember("hi!");
        java.lang.Class<?> wildcardClass8 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1345");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1346");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        int int8 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) (short) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1347");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
    }

    @Test
    public void test1348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1348");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        int int7 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7 == 0);
    }

    @Test
    public void test1349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1349");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1350");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1351");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass((int) (byte) 0);
        // The following exception was thrown during execution in test generation
        try {
            int int10 = library0.getTotalPenalty("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
    }

    @Test
    public void test1352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1352");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        java.lang.Class<?> wildcardClass10 = strList9.getClass();
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test1353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1353");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1354");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1355");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        int int7 = library0.getTotalPenalty("hi!");
        int int9 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7 == 0);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1356");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass((int) (byte) 1);
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (short) 10, (int) (byte) 10, (int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1357");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) 'a');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1358");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1359");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (byte) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (short) -1, 0, (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1360");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        library0.timePass((int) 'a');
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1361");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1362");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            int int11 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1363");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.addBook("hi!", (int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (-1));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1364");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) ' ');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1365");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            int int9 = library0.getTotalPenalty("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1366");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '#');
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
    }

    @Test
    public void test1367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1367");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1368");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1369");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1370");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1371");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass((int) '#');
        library0.timePass((int) (byte) 10);
        library0.borrow("hi!", "hi!");
        int int28 = library0.getTotalPenalty("hi!");
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 274000 + "'", int28 == 274000);
    }

    @Test
    public void test1372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1372");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass((int) '#');
        library0.timePass((int) (byte) 10);
        library0.borrow("hi!", "hi!");
        java.lang.Class<?> wildcardClass27 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test1373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1373");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((int) (byte) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1374");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1375");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.lang.Class<?> wildcardClass8 = library0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1376");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1377");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        int int15 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1378");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        int int13 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '4');
        // The following exception was thrown during execution in test generation
        try {
            int int17 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1379");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", 10);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.timePass((int) (byte) 1);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1380");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        library0.addProfMember("hi!");
        library0.timePass(1);
    }

    @Test
    public void test1381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1381");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        library0.timePass((int) (short) 0);
    }

    @Test
    public void test1382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1382");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) ' ');
        library0.timePass((int) (byte) 0);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) -1, (int) (short) 10, (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1383");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1384");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1385");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addBook("hi!", (int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1386");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int11 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1387");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1388");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.lang.Class<?> wildcardClass13 = strList12.getClass();
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1389");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        int int12 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12 == 0);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1390");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        java.lang.Class<?> wildcardClass11 = library0.getClass();
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1391");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1392");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        java.util.List<java.lang.String> strList23 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (byte) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertNotNull(strList23);
    }

    @Test
    public void test1393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1393");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1394");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1395");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(100);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1396");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1397");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1398");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        java.util.List<java.lang.String> strList1 = library0.availableTitles();
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList1);
    }

    @Test
    public void test1399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1399");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1400");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList25 = library0.availableTitles();
        java.util.List<java.lang.String> strList26 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addProfMember("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList25);
        org.junit.Assert.assertNotNull(strList26);
    }

    @Test
    public void test1401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1401");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.lang.Class<?> wildcardClass8 = strList7.getClass();
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test1402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1402");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1403");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        int int8 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1404");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 10);
        int int13 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1405");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        java.util.List<java.lang.String> strList1 = library0.availableTitles();
        library0.addReference("hi!", 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList1);
    }

    @Test
    public void test1406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1406");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addBook("hi!", (int) (byte) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (short) 0, 0, (-76000));
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1407");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        int int16 = library0.getTotalPenalty("hi!");
        library0.addReference("hi!", (int) (byte) 100);
        java.lang.Class<?> wildcardClass20 = library0.getClass();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16 == 0);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test1408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1408");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        library0.addBook("hi!", (int) '4');
        java.lang.Class<?> wildcardClass19 = library0.getClass();
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test1409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1409");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
    }

    @Test
    public void test1410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1410");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1411");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '4');
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        java.lang.Class<?> wildcardClass18 = library0.getClass();
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test1412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1412");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addStudentMember("hi!", "hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1413");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 10);
        int int13 = library0.getTotalPenalty("hi!");
        library0.borrow("hi!", "hi!");
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1414");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1415");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1416");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", (int) (byte) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 0, (int) (short) 0, (int) (short) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1417");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        library0.timePass(100);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1418");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        library0.timePass(0);
        java.lang.Class<?> wildcardClass13 = library0.getClass();
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1419");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1420");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) (byte) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1421");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.addProfMember("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.timePass((int) (short) -1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot go back in time");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
    }

    @Test
    public void test1422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1422");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass(10);
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        int int24 = library0.getTotalPenalty("hi!");
        int int26 = library0.getTotalPenalty("hi!");
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-76000) + "'", int24 == (-76000));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-76000) + "'", int26 == (-76000));
    }

    @Test
    public void test1423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1423");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        library0.extend("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1424");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1425");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1426");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
    }

    @Test
    public void test1427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1427");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 0, (int) (byte) 10, (int) (short) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1428");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        library0.timePass((int) (short) 0);
        int int13 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) 'a');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1429");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1430");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        java.lang.Class<?> wildcardClass16 = library0.getClass();
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test1431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1431");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            int int17 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1432");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.addBook("hi!", (int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) (byte) 0, (int) (byte) 1, (int) (short) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1433");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        java.util.List<java.lang.String> strList23 = library0.availableTitles();
        java.util.List<java.lang.String> strList24 = library0.availableTitles();
        java.util.List<java.lang.String> strList25 = library0.availableTitles();
        int int27 = library0.getTotalPenalty("hi!");
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertNotNull(strList23);
        org.junit.Assert.assertNotNull(strList24);
        org.junit.Assert.assertNotNull(strList25);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27 == 0);
    }

    @Test
    public void test1434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1434");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(0);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
    }

    @Test
    public void test1435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1435");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        library0.addBook("hi!", (int) '4');
        int int11 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        library0.timePass((int) 'a');
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
    }

    @Test
    public void test1436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1436");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1437");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1438");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass((int) '4');
        library0.timePass(1);
        library0.timePass(100);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1439");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1440");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        library0.timePass(100);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
    }

    @Test
    public void test1441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1441");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        library0.addMagazine("hi!", 10, (int) (short) 1, (int) (byte) 1);
        java.lang.Class<?> wildcardClass13 = library0.getClass();
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1442");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) '4');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1443");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) (short) 0);
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        int int15 = library0.getTotalPenalty("hi!");
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1444");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        library0.timePass((int) '4');
        int int9 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '4');
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
    }

    @Test
    public void test1445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1445");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        library0.timePass((int) (short) 0);
        int int13 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList14 = library0.availableTitles();
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
        org.junit.Assert.assertNotNull(strList14);
    }

    @Test
    public void test1446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1446");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        library0.timePass((int) '#');
        int int15 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (-1), (int) (byte) 1, (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative publication year");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
    }

    @Test
    public void test1447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1447");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        int int12 = library0.getTotalPenalty("hi!");
        library0.addMagazine("hi!", (int) '4', (int) (short) 1, (int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12 == 0);
    }

    @Test
    public void test1448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1448");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList18 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
        org.junit.Assert.assertNotNull(strList18);
    }

    @Test
    public void test1449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1449");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1450");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("hi!", (int) ' ', (int) (short) 0, (int) (short) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Magazine with zero or negative number");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1451");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass((int) '#');
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1452");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        library0.timePass((int) '4');
        library0.timePass(1);
        java.lang.Class<?> wildcardClass15 = library0.getClass();
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test1453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1453");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        library0.addBook("hi!", 100);
        int int8 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass(0);
        org.junit.Assert.assertNotNull(strList3);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertNotNull(strList9);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1454");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        int int9 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1455");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.addReference("hi!", 100);
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            int int18 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1456");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.addMagazine("hi!", (int) ' ', (int) (byte) 1, (int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1457");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("hi!", (int) (byte) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Negative or zero copies of a document cannot be added");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(strList11);
    }

    @Test
    public void test1458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1458");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        int int8 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        library0.addReference("hi!", (int) (short) 1);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertNotNull(strList9);
    }

    @Test
    public void test1459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1459");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1460");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass((int) ' ');
        library0.addReference("hi!", 1);
    }

    @Test
    public void test1461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1461");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addReference("hi!", 10);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addMagazine("", (int) (byte) 10, (int) (byte) 100, (int) (short) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1462");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        library0.addReference("hi!", (int) (byte) 10);
        java.util.List<java.lang.String> strList9 = library0.availableTitles();
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        java.lang.Class<?> wildcardClass11 = library0.getClass();
        org.junit.Assert.assertNotNull(strList9);
        org.junit.Assert.assertNotNull(strList10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test1463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1463");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", (int) (byte) 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1464");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1465");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        int int19 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList17);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19 == 0);
    }

    @Test
    public void test1466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1466");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1467");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        library0.timePass((int) ' ');
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1468");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        int int13 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addBook("", 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1469");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1470");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1471");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
    }

    @Test
    public void test1472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1472");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.addProfMember("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        library0.timePass(274000);
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
    }

    @Test
    public void test1473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1473");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.addMagazine("hi!", (int) (byte) 100, (int) (short) 10, 1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (short) 10);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1474");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(274000);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1475");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addMagazine("hi!", (int) (byte) 1, (int) 'a', 1);
        library0.timePass(1);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1476");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.timePass(10);
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1477");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty member name not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1478");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1479");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList11 = library0.availableTitles();
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.lang.Class<?> wildcardClass13 = library0.getClass();
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertNotNull(strList11);
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test1480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1480");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) (byte) 1);
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
    }

    @Test
    public void test1481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1481");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.borrow("hi!", "hi!");
        library0.timePass(10);
        java.util.List<java.lang.String> strList22 = library0.availableTitles();
        java.util.List<java.lang.String> strList23 = library0.availableTitles();
        org.junit.Assert.assertNotNull(strList22);
        org.junit.Assert.assertNotNull(strList23);
    }

    @Test
    public void test1482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1482");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        library0.extend("hi!", "hi!");
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1483");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        java.util.List<java.lang.String> strList13 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass((int) (short) 1);
        library0.borrow("hi!", "hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", 1);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList13);
    }

    @Test
    public void test1484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1484");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '4');
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList17);
    }

    @Test
    public void test1485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1485");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.timePass((int) (byte) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Empty student ID is not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1486");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass(1);
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (short) 100);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }

    @Test
    public void test1487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1487");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addBook("hi!", (int) (short) 1);
        java.util.List<java.lang.String> strList4 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        library0.addProfMember("hi!");
        library0.timePass((int) '#');
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        library0.timePass((int) ' ');
        org.junit.Assert.assertNotNull(strList4);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList12);
    }

    @Test
    public void test1488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1488");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.timePass((int) ' ');
        library0.addStudentMember("hi!", "hi!");
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        library0.timePass((int) (short) 10);
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList6);
    }

    @Test
    public void test1489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1489");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass((int) (short) 1);
        library0.timePass((int) ' ');
        library0.addBook("hi!", (int) (short) 1);
        // The following exception was thrown during execution in test generation
        try {
            library0.addStudentMember("hi!", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateMemberEx; message: Member with the same name exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
    }

    @Test
    public void test1490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1490");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        int int11 = library0.getTotalPenalty("hi!");
        library0.timePass((int) '#');
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11 == 0);
    }

    @Test
    public void test1491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1491");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) (short) 100);
        // The following exception was thrown during execution in test generation
        try {
            int int12 = library0.getTotalPenalty("");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1492");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        java.util.List<java.lang.String> strList3 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.extend("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList3);
    }

    @Test
    public void test1493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1493");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        java.util.List<java.lang.String> strList5 = library0.availableTitles();
        library0.timePass(0);
        library0.timePass(100);
        library0.timePass(0);
        library0.addBook("hi!", (int) (byte) 1);
        library0.timePass((int) (short) 0);
        java.util.List<java.lang.String> strList17 = library0.availableTitles();
        java.util.List<java.lang.String> strList18 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find member to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList5);
        org.junit.Assert.assertNotNull(strList17);
        org.junit.Assert.assertNotNull(strList18);
    }

    @Test
    public void test1494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1494");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.addBook("hi!", (int) (byte) 100);
        java.util.List<java.lang.String> strList6 = library0.availableTitles();
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        int int9 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList10 = library0.availableTitles();
        library0.timePass((int) '#');
        org.junit.Assert.assertNotNull(strList6);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9 == 0);
        org.junit.Assert.assertNotNull(strList10);
    }

    @Test
    public void test1495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1495");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList7 = library0.availableTitles();
        java.util.List<java.lang.String> strList8 = library0.availableTitles();
        library0.timePass((int) '#');
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertNotNull(strList7);
        org.junit.Assert.assertNotNull(strList8);
    }

    @Test
    public void test1496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1496");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        int int6 = library0.getTotalPenalty("hi!");
        int int8 = library0.getTotalPenalty("hi!");
        int int10 = library0.getTotalPenalty("hi!");
        library0.timePass(10);
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.borrow("", "hi!");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Cannot find document to borrow");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6 == 0);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8 == 0);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10 == 0);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
    }

    @Test
    public void test1497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1497");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        int int13 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) (short) 0);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13 == 0);
    }

    @Test
    public void test1498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1498");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass((int) (short) 100);
        int int15 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList16 = library0.availableTitles();
        // The following exception was thrown during execution in test generation
        try {
            library0.returnDocument("hi!", "");
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: The document is not in member's loan");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15 == 0);
        org.junit.Assert.assertNotNull(strList16);
    }

    @Test
    public void test1499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1499");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (byte) 100);
        library0.timePass(1);
        library0.borrow("hi!", "hi!");
        library0.returnDocument("hi!", "hi!");
        library0.timePass((int) (byte) 10);
        library0.timePass(100);
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("hi!", (int) (short) 10);
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.DuplicateDocumentEx; message: Document with the same title exists");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
    }

    @Test
    public void test1500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test1500");
        ir.ramtung.impl1.Library library0 = new ir.ramtung.impl1.Library();
        library0.addProfMember("hi!");
        library0.timePass(100);
        library0.timePass(0);
        library0.addReference("hi!", (int) 'a');
        library0.timePass((int) (short) 100);
        java.util.List<java.lang.String> strList12 = library0.availableTitles();
        int int14 = library0.getTotalPenalty("hi!");
        java.util.List<java.lang.String> strList15 = library0.availableTitles();
        int int17 = library0.getTotalPenalty("hi!");
        // The following exception was thrown during execution in test generation
        try {
            library0.addReference("", (int) '#');
            org.junit.Assert.fail("Expected exception of type ir.ramtung.impl1.InvalidArgumentEx; message: Documents with empty title are not allowed");
        } catch (ir.ramtung.sts01.LibraryException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(strList12);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14 == 0);
        org.junit.Assert.assertNotNull(strList15);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17 == 0);
    }
}

